
<!DOCTYPE HTML>  
	<html>
		<head>
			<title>
				Add Patient
			</title>
			<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		</head>
		<body>  

			<?php
			//link sylesheet css			
			echo "<link rel='stylesheet' type='text/css' href='style.css' />";
			
			// define variables and set to empty values
			$nameErr = $birthdayErr = $genderErr = $addressErr = "";
			$name = $birthday = $gender = $address = "";

					if ($_SERVER["REQUEST_METHOD"] == "POST") {
					  if (empty($_POST["name"])) {
						$nameErr = "Name is required";
					  } else {
						$name = test_input($_POST["name"]);
					  }
					  
					  if (empty($_POST["birthday"])) {
						$birthdayErr = "Birthday number is required";
					  } else {
						$birthday = test_input($_POST["birthday"]);
					  }
						
					  if (empty($_POST["address"])) {
						$address = "";
					  } else {
						$address = test_input($_POST["address"]);
					  }

					  if (empty($_POST["gender"])) {
						$genderErr = "Gender is required";
					  } else {
						$gender = test_input($_POST["gender"]);
					  }
					}

			function test_input($data) {
			  $data = trim($data);
			  $data = stripslashes($data);
			  $data = htmlspecialchars($data);
			  return $data;
			}
			?>

		
				<div class = "form">
					<h1>Patient Form</h1>
				
				
				<p><span class="error">* required fields</span></p>
					<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
							  <span class="error">*<?php echo $nameErr;?>
							  Name: <input type="text" name="name">
							  </span>
							  <span class="error">*<?php echo $birthdayErr;?>
							  Birthday: <input type="date" name="birthday">
							  </span>
							 
							 <span class="error"><?php echo $addressErr;?>
							  Address: <input type="text" name="address">
							  </span>
							  
							  <span class="error">*<?php echo $genderErr;?>				
							  Gender:
							  <select name="gender"  >
								  <option value="Male">M</option>
								  <option value="Female">F</option>
								  <option value="Unknown">Unknown</option>
							  </select>
							  
							  </span>
							  <br><br><br>
							
								<div id= "button-container">
							  <input type="submit" name="submit" value="Submit" align=middle onclick="alert('New Patient Added. Please scroll down for patient details.')">   
								</div>
					</form>
				</div>

				<div class = "details">
					<?php
						echo "<h2>Patient Details:</h2>";
						echo "Name: ", $name;
						echo "<br>";
						echo "Birthday: ",$birthday;
						echo "<br>";
						echo "Address: ",$address;
						echo "<br>";
						echo "Gender: ",$gender;
					?>
				</div>

		</body>
		</html>